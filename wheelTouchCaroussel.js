/**
 *     wheelTouchScroller
 *     scroller vertical or horizontal
 *
 *     <div>
 *     	<div>
 *     	<element></element>
 *     </div>
 *     </div>
 *     
 *     @method 	wheelTouchScroller()
 *     @param     	{string}	 			opt :  "v" = vertical, "h" = horizontal, "kill" = unbind() and rebuilt, default vertical
 *     @return 		{animation}    	scrolling element 
 */
(function($) {
			
	$.fn.wheelTouchCaroussel = function(opt,spd) {
		
		var myPnl = this;
		var myGrp = this.children();
		switch (myGrp.length) {
		case !1:
			console.log("Error ->",myGrp,": to many elements");
			console.log("Error ->",myPnl,"most need one container element" );
			alert(Error('Check in the console.log for more info'));
			break;

		default:
			break;
		}
		var myElem = myGrp.children();
		function rebuild(){
			var minW = 0;
			myElem.map(function(i, e) {
				minW += $(e).innerWidth();
				//console.log(minW);
			});

			myGrp.animate({
				width: minW
				},
				{
					duration: 0,
					done: function() {
						//console.log("animation fini à ",minW);
					}
				});
		}
		rebuild();
		
		var speed = 0;
		if (spd) {
			speed = spd;
		}else{
			speed = 50;
		}
		var dirOption = null;
		var pnl;
		var grp;
		var parcour = 0;
		var last = 0;
		var diff = 0;
		var delta = 0;
		var deltaY_int = 0;
		switch(opt){
			case "h": 		pnl = myPnl.width();
									myGrp.height('auto');
									grp = parseFloat( myGrp.outerWidth() + ( parseFloat( myGrp.css('padding') /2 ) ));
									if (!grp) {
										grp = parseFloat( myGrp.innerWidth() + parseInt( myGrp.css('border-width') ) + ( parseFloat( myGrp.css('padding') )/2 ) );
									}
					 				dirOption = "left";
									break;
									
			case "v": 		pnl = myPnl.height();
									myGrp.width('auto');
									grp = parseFloat( myGrp.outerHeight() + ( parseFloat( myGrp.css('padding') /2 ) ));
									if (!grp) {
										grp = parseFloat( myGrp.innerHeight() + parseInt( myGrp.css('border-width') ) + ( parseFloat( myGrp.css('padding') )/2 ) );
									}
					 				dirOption = "top";
									break;
									
			case "kill": 	myGrp.css({
										top: 0,
										left: 0
									});
									return this.unbind();
									break;
									
			default: 		pnl = myPnl.width();
									myGrp.height('auto');
									grp = parseFloat( myGrp.outerWidth() + ( parseFloat( myGrp.css('padding') /2 ) ));
									if (!grp) {
										grp = parseFloat( myGrp.innerWidth() + parseInt( myGrp.css('border-width') ) + ( parseFloat( myGrp.css('padding') )/2 ) );
									}
									dirOption = "left";
									break;
		}

		var VAL =0;
		var LIMIT = 5;
		var M = 0;
		var T= 0;
		if (parseFloat(grp-pnl)) {
			M = parseFloat(grp-pnl);
		} 
		if (parseFloat(M/LIMIT)) {
			T = parseFloat(M/LIMIT);
		}
		var D=0;
		var moveStart = 0;
		var moveEvent = 0;
				
		myPnl.on('mousewheel touchstart touchmove wheel', function(e) {
			e.preventDefault();
			if(last){
				diff = e.timeStamp - last;
			}
			last = e.timeStamp;
			if (e.type == 'mousewheel' || e.type == 'wheel') {
				parcour = parseFloat((delta/(diff*speed)).toFixed(3));
			} else {
				parcour = parseFloat((delta/(diff*10)).toFixed(3));
			}
			
			if(!parcour){
				parcour = 0;
			}

			switch(e.type){
				case "mousewheel":
					if (e.originalEvent.wheelDeltaY) {
						delta = -e.originalEvent.wheelDeltaY;
					} else {
						delta = e.originalEvent.deltaY;
					}
					deltaY_int = parcour;
					console.log(actionMove());
					break;

				case "wheel":
					delta = e.originalEvent.deltaY;
					deltaY_int = parcour;
					console.log("start : ",parcour);
					break;
					
				case "touchstart":
					last = e.timeStamp;
					moveStart = 0;
					
				case "touchmove":
					if(moveStart <= 0){
						if(dirOption == "top"){
							moveEvent = e.originalEvent.touches[0].screenY;
						}
						if(dirOption == "left"){
							moveEvent = e.originalEvent.touches[0].screenX;
						}
					}
					if(dirOption == "top"){
						delta = (e.originalEvent.changedTouches[0].screenY - moveEvent)*2.5;
					};
					if(dirOption == "left"){
						delta = e.originalEvent.changedTouches[0].screenX - moveEvent;
					}

					if (delta>=0){
						deltaY_int = -parcour;
					}else{
						deltaY_int = -parcour;
					}
					moveStart ++;
					if(dirOption == "top"){
							moveEvent = e.originalEvent.touches[0].screenY;
						}
						if(dirOption == "left"){
							moveEvent = e.originalEvent.touches[0].screenX;
						}
					e.stopPropagation();
					break;

			}

			function actionMove() {
				VAL +=  parseFloat(deltaY_int/LIMIT);
				if(VAL >= LIMIT){
					VAL = LIMIT;
				}
				if(VAL <= 0){
					VAL = 0;
				}
				D = parseFloat(T*VAL);
				myGrp.css(dirOption,-D);
			}
			
			actionMove();
			 //	******************************
			 //	test de retour
			 //	
				/*console.log("velocity = ",diff*ts);
			 	console.log("force = ",delta);
			 	console.log("delta = ",delta);
			 	console.log("moveEvent = ",moveEvent);
			 	console.log("myPnl = ",myPnl);
			 	console.log("pnl = ",pnl);
			 	console.log("grp = ",grp);
			 	console.log("LIMIT = ",LIMIT);
			 	console.log("M = ",M);
			 	console.log("T = ",T);
			 	console.log("D = ",D);
			 	console.log("omg = ",omg);
			 	console.log("gmo = ",gmo);
				 console.log("VAL = ",VAL);*/
			 //	******************************
		});
	};
			
})(jQuery);