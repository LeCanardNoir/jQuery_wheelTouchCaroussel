wheelTouchCaroussel
================

jQuery plugin

A generic horizontal or vertical slider caroussel compatible with mouse wheel and touch event.


USAGE:
=============================================

element:
	div, or else (you can call with his CLASS or ID.
	need one children element (div is recommended).

						
element -> children:
	put all you want here (img, p, h1,..... else) without margin just use padding.
						

<code>$( [element] ).wheelTouchCaroussel( [opt, spd] );</code>

opt: {string} optional (default: "h")<br>
	"v" 	= vertical<br>
	"h" 	= horizontal<br>
	"kill"	= unbind all and reset<br><br>
spd : {number} optional (default: 50) effective just on mousewheel event not touch<br>
1 = full speed (very fast)
50 = preferred speed it's more intuitive
<50 = more slower
<100+ = very slow

test link:
http://devlcn.shost.ca/jQuery/wtc/



tets branch